var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser"); // simplifies access to request body

var app = express();  // make express app
var http = require('http').Server(app);  // inject app into the server

app.use(express.static(__dirname + '/assets'));
//app.use(express.static(__dirname + '/views'));

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// 2 create an array to manage our entries
var entries = [];
app.locals.entries = entries; // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  response.render("HomePage");
});
app.get("/HomePage", function (request, response) {
  response.render("HomePage");
});
app.get("/about", function (request, response) {
  response.render("HomePage");
});
app.get("/TaxCalculator", function (request, response) {
  response.render("TaxCalculator");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/guestbook", function (request, response) {
  response.render("index");
});
app.get("/contact", function (request, response) {
  response.render("Contact");
});
app.post("/contact", function(request, response){
  var api_key = 'key-713648ee79e904ac80e7a3b2989b854c';
  var domain = 'sandbox263fd28356f74a88a0a5709a7df910c2.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Mail Gun TustDady <postmaster@sandbox263fd28356f74a88a0a5709a7df910c2.mailgun.org>',
    to: 'unclebenwasshot@gmail.com',
    subject: request.body.username,
    text: request.body.question
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error){
     // response.render("Contact");
        response.send("Mail Sent");
    }
    else{
      //response.render("HomePage");
      response.send("Mail not sent");
    }
  });
});
// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  console.log(request.body);
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");  // where to go next? Let's go to the home page 🙂
});


// Listen for an application request on port 8081 & notify the developer
//http.listen(8081, function () {
 // console.log('Guestbook app listening on http://127.0.0.1:8081/');
//});




// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('A03 project listening on http://127.0.0.1:8081/');
});