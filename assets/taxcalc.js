function taxAmount() {
    
        var income = document.getElementById("income").value;
        var taxpercent = document.getElementById("taxpercent").value;

        let result = calculate(income,taxpercent);
        $("#output").html("Your income after tax is $" + result);

    }

function calculate(income,taxpercent){
              
    if (income <= 0 || taxpercent <= 0) {
        throw "Income and tax percentage cannot be 0 or less";
        alert("Income and tax percentage cannot be zero or less.");

    }

    else if (isNaN(income) || isNaN(taxpercent)){
        throw "Both income and tax percentage have to be numbers";
        alert("Income and tax percentage has to be a number");
    }

    else {
        let total = income - ((income * (taxpercent / 100)));
        return total;
    }
}